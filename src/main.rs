use std::env;
use std::process::{
	Command,
	Stdio
};

fn main() {
	let mut args = env::args_os().skip(1);
	
	if let Some(command) = args.next() {
		let mut command = Command::new(command);
		let envs = env::vars_os();
		
		command.args(args);
		command.envs(envs);
		command.stdin(Stdio::null());
		command.stdout(Stdio::null());
		command.stderr(Stdio::null());
		
		match command.spawn() {
			Ok(_) => (),
			Err(e) => eprintln!("{}", e)
		}
	}
}
